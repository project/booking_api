<?php

namespace Drupal\booking_api\Plugin\views\field;

use Drupal\views\Plugin\views\field\EntityLabel;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to display entity label optionally linked to entity page.
 *
 * @ViewsField("booking_api_booked_entity_field")
 */
class BookedEntityField extends EntityLabel {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    $this->definition['entity type field'] = 'entity_type_id';
    parent::init($view, $display, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    return parent::render($values);
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(&$values) {
    $entity_ids_per_type = [];

    foreach ($values as $value) {
      if ($type = $this->getValue($value, 'entity_type_id')) {
        $id = $this->getValue($value);
        $entity_ids_per_type[$type][$id] = $id;
      }
    }

    foreach ($entity_ids_per_type as $type => $ids) {
      $this->loadedReferencers[$type] = $this->entityTypeManager->getStorage($type)->loadMultiple($ids);
    }
  }

}
