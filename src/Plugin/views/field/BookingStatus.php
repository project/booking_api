<?php

namespace Drupal\booking_api\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to display booking status label.
 *
 * @ViewsField("booking_api_booking_status")
 */
class BookingStatus extends FieldPluginBase {

  /**
   * Called to add the field to a query.
   */
  public function query() {

  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {
    if ($this->options['relationship'] != 'none' && isset($row->_relationship_entities[$this->options['relationship']])) {
      $booking = $row->_relationship_entities[$this->options['relationship']];
    }
    elseif ($this->options['relationship'] == 'none' && !isset($row->_relationship_entities[$this->options['relationship']])){
      $booking = $row->_entity;
    }
    return $booking->get('status')->entity->label();
  }


}
