<?php

namespace Drupal\booking_api\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\booking_api\Service\BookingManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource to handle bookings.
 *
 * @RestResource(
 *   id = "booking_rest_resource",
 *   label = @Translation("Booking rest resource"),
 *   uri_paths = {
 *     "create" = "/api/booking",
 *     "canonical" = "/api/booking"
 *   }
 * )
 */
class BookingRestResource extends ResourceBase {

  /**
   * The current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Entity Type Manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity Field Manager instance.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity being created or modified.
   *
   * @var \Drupal\core\Entity\FieldableEntityInterface
   */
  protected $entity;

  /**
   * Datetime service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The booking manager service.
   *
   * @var \Drupal\booking_api\Service\BookingManagerInterface
   */
  protected $bookingManager;

  /**
   * The current request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a new BookingRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Datetime service.
   * @param \Drupal\booking_api\Service\BookingManagerInterface $bookingManager
   *   The booking manager service.
   * @param \Symfony\Component\HttpFoundation\Request $currentRequest
   *   The current request.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManager,
    TimeInterface $time,
    BookingManagerInterface $bookingManager,
    Request $currentRequest
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->time = $time;
    $this->bookingManager = $bookingManager;
    $this->currentRequest = $currentRequest;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('iko_messages_api'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('datetime.time'),
      $container->get('booking_api.booking_manager'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function permissions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRouteRequirements($method) {
    return [
      '_access' => 'TRUE',
    ];
  }

  /**
   * Common access callback for every operation.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  protected function checkAccess() {
    if (!$this->currentUser->hasPermission('make bookings') && !$this->currentUser->hasPermission('book for all')) {
      throw new AccessDeniedHttpException("The current user doesn't have any booking permissions.");
    }
  }

  /**
   * Request data validation.
   *
   * @param array $data
   *   The entity field data to validate.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   */
  protected function validatePostData(array &$data) {
    $errors = [];

    // Edition.
    if (isset($data['id'])) {
      $this->entity = $this->entityTypeManager->getStorage('booking_instance')->load($data['id']);
      if (!$this->entity) {
        $errors[] = [
          'code' => 1,
          'message' => sprintf('Booking with the specified ID (%d) doesn\'t exist.', $data['id']),
        ];
      }
      $data['entity_type_id'] = $this->entity->entity_type_id[0]->getValue()['value'];
      $data['entity_id'] = $this->entity->entity_id[0]->getValue()['value'];
    }

    // Creation.
    else {
      foreach ($this->entityFieldManager->getFieldDefinitions('booking_instance', 'booking_instance') as $field_id => $definition) {
        if ($definition->isRequired() && empty($data[$field_id])) {
          $errors[] = [
            'code' => 2,
            'message' => sprintf('Required field %s (%s) is missing.', $field_id, $definition->getLabel()),
          ];
        }
      }
    }

    // We're sure we have the booked entity data now.
    if (empty($errors)) {
      if ($booked_entity = $this->entityTypeManager->getStorage($data['entity_type_id'])->load($data['entity_id'])) {
        $booking_errors = $this->bookingManager->validateBooking($data, $booked_entity, !empty($data['id']));
        if (!empty($booking_errors)) {
          foreach ($booking_errors as $code => $message) {
            $errors[] = [
              'code' => $code,
              'message' => (string) $message,
            ];
          }
        }
      }
      else {
        $errors[] = [
          'code' => 3,
          'message' => "The entity associated with this booking doesn't exist.",
        ];
      }
    }

    if (!empty($errors)) {
      return $this->invalidRequest(json_encode($errors));
    }
  }

  /**
   * Create / update a booking.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post(array $data) {
    $this->checkAccess();

    $validate_only = FALSE;
    if (isset($data['validate_only'])) {
      $validate_only = $data['validate_only'];
    }

    if (empty($data['id'])) {
      $data['created'] = $this->time->getRequestTime();
    }
    $data['changed'] = $this->time->getRequestTime();

    $this->validatePostData($data);

    if (!$validate_only) {
      if (!isset($this->entity)) {
        $this->entity = $this->entityTypeManager->getStorage('booking_instance')->create($data);
        $this->entity->save();
        $message = [
          'message' => 'Booking created.',
          'booking_id' => $this->entity->id()
        ];
      }
      else {
        foreach ($data as $param => $value) {
          if (isset($this->entity->{$param})) {
            $this->entity->{$param}->setValue($value);
          }
        }
        $this->entity->save();
        $message = 'Booking updated.';
      }
    }
    else {
      $message = 'Booking validated successfully.';
    }

    return new ModifiedResourceResponse($message, 200);
  }

  /**
   * Get bookings for an entity.
   */
  public function get() {
    $this->checkAccess();

    $query_params = $this->currentRequest->query->all();

    // Merge in defaults.
    $query_params += [
      'entity_type_id' => 'node',
      'offset' => 0,
      'limit' => 50,
    ];
    if (!$this->currentUser->hasPermission('book for all')) {
      $query_params['user_id'] = $this->currentUser->id();
    }

    $pager_data = [
      'offset' => 0,
      'limit' => 50,
    ];
    unset($query_params['offset']);
    unset($query_params['limit']);

    // Build conditions array.
    $conditions = [];
    foreach ($query_params as $param => $value) {
      if (preg_match('#^([><=]{1,2}|)([0-9]+)$#', $value, $matches)) {
        if (!in_array($matches[1], ['>', '<', '>=', '<=', '='])) {
          return $this->invalidRequest(sprintf('Invalid operator in %s condition.', $param));
        }
        $conditions[] = [$param, $matches[2], $matches[1]];
      }
      else {
        $values = explode(',', $value);
        if (count($values) > 1) {
          $conditions[] = [$param, $values];
        }
        else {
          $conditions[] = [$param, $value];
        }
      }
    }

    $result = $this->bookingManager->getBookingsByQuery($conditions, $pager_data, TRUE);

    $response = new ResourceResponse($result);
    $response->addCacheableDependency([
      '#cache' => [
        'max-age' => 0,
      ],
    ]);

    return $response;
  }

  /**
   * Delete a booking.
   */
  public function delete(array $data) {
    $this->checkAccess();

    if (!isset($data['id'])) {
      return $this->invalidRequest('Booking ID must be provided.');
    }
    if (!$booking = $this->entityTypeManager->getStorage('booking_instance')->load($data['id'])) {
      return $this->invalidRequest('Invalid booking ID provided.');
    }

    $owner = ($this->currentUser->id() == $booking->user_id[0]->getValue()['target_id']);
    if (!$this->currentUser->hasPermission('book for all')) {
      if (!$owner) {
        return $this->invalidRequest("This booking doesn't belong to the current user.");
      }

      // Don't allow to delete past or pending bookings.
      elseif ($booking->from[0]->getValue()['value'] <= $this->time->getRequestTime()) {
        return $this->invalidRequest("Can only delete future bookings.");
      }
    }

    $booking->delete();
    return new JsonResponse('Booking deleted.', 200);
  }

  /**
   * Bad request response handler.
   */
  protected function invalidRequest($message) {
    return new JsonResponse([
      'status' => 'error',
      'message' => $message,
    ], 400);
  }

}
