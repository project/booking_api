<?php

namespace Drupal\booking_api\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the 'booking_parameters' entity field type.
 *
 * @FieldType(
 *   id = "booking_parameters",
 *   label = @Translation("Booking parameters"),
 *   description = @Translation("Contains parameters that determine booking possibilities for this entity."),
 *   category = @Translation("Booking"),
 *   default_widget = "booking_parameters_widget",
 *   default_formatter = "booking_parameters_overview",
 *   list_class = "\Drupal\booking_api\Plugin\Field\BookingParametersFieldItemList",
 *   cardinality = 1,
 * )
 */
class BookingParameters extends FieldItemBase {

  const DEFAULTS = [
    'enabled' => TRUE,
    'max_bookings' => 0,
    'max_overlapping_bookings' => 1,
    'max_bookings_per_user' => 0,
    'start_date' => 0,
    'end_date' => 0,
    'booking_item_length' => 86400,
    'default_status' => '',
  ];

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'enabled' => [
          'type' => 'int',
          'size' => 'tiny',
          'not null' => TRUE,
        ],
        'max_bookings' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'max_overlapping_bookings' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'max_bookings_per_user' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'start_date' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'end_date' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'booking_item_length' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'default_status' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['enabled'] = DataDefinition::create('boolean')
      ->setLabel(t('Is booking enabled for this entity?'))
      ->setRequired(TRUE);

    $properties['max_bookings'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Maximum total number of bookings for the entity'))
      ->setSetting('unsigned', TRUE);

    $properties['max_overlapping_bookings'] = DataDefinition::create('integer')
      ->setLabel(t('Maksimum overlapping bookings'))
      ->setSetting('unsigned', TRUE);

    $properties['max_bookings_per_user'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Maximum total number of bookings per single user for the entity'))
      ->setSetting('unsigned', TRUE);

    $properties['start_date'] = DataDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Start date for the bookings'))
      ->setSetting('unsigned', TRUE);

    $properties['end_date'] = DataDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('End date for the bookings'))
      ->setSetting('unsigned', TRUE);

    $properties['booking_item_length'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Booking item duration in seconds'))
      ->setSetting('unsigned', TRUE);

    $properties['booking_item_length'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Booking item duration in seconds'))
      ->setSetting('unsigned', TRUE);

    $properties['default_status'] = DataReferenceTargetDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Booking status entity ID'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    // There is no main property in case of this field but..
    return 'enabled';
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // This field is never empty in Drupal meaning.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    parent::getValue();
    $set_defaults = $this->getFieldDefinition()->getDefaultValueLiteral();
    if (is_array($set_defaults) && isset($set_defaults[0])) {
      $this->values += $set_defaults[0];
    }
    $this->values += self::DEFAULTS;
    return $this->values;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Merge in defaults.
    $set_defaults = $this->getFieldDefinition()->getDefaultValueLiteral();
    if (is_array($set_defaults) && isset($set_defaults[0])) {
      $values += $set_defaults[0];
    }
    $values += self::DEFAULTS;
    parent::setValue($values, $notify);
  }

}
