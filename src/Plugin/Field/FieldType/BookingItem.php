<?php

namespace Drupal\booking_api\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\booking_api\Exception\BookingApiException;

/**
 * Defines the 'bookings' entity field type.
 *
 * Supported settings (below the definition's 'settings' key) are:
 * - allow_multiple: Allow multiple bookings from the same user?
 *
 * DEPRECATED: this field should no longer be used in favour of
 * new booking node and user tabs.
 *
 * @FieldType(
 *   id = "bookings",
 *   label = @Translation("Bookings field"),
 *   description = @Translation("An entity field containing references to booking instance entities."),
 *   category = @Translation("Reference"),
 *   default_widget = "booking_widget",
 *   default_formatter = "bookings_table",
 *   list_class = "\Drupal\booking_api\Plugin\Field\BookingFieldItemList",
 *   no_ui = TRUE,
 * )
 */
class BookingItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $defaults = parent::defaultStorageSettings();
    $defaults['target_type'] = 'booking_instance';
    $defaults['user_maximum'] = 1;
    $defaults['statuses'] = [];
    $defaults['cutaway_period'] = 365;
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element['user_maximum'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum number of bookings per user'),
      '#step' => 1,
      '#min' => 0,
      '#default_value' => $this->getSetting('user_maximum'),
      '#required' => TRUE,
      '#disabled' => $has_data,
    ];

    $available_statuses = \Drupal::service('entity_type.manager')->getStorage('booking_status')->loadMultiple();
    $status_options = [];
    foreach ($available_statuses as $id => $booking_status) {
      $status_options[$id] = $booking_status->label();
    }
    $element['statuses'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#options' => $status_options,
      '#title' => $this->t('Available statuses'),
      '#description' => $this->t('Select which statuses will be available on this field. For statuses management go @link. Existing bookings with other statuses will not be shown or editable by the field UI.', [
        '@link' => Link::fromTextAndUrl($this->t('here'), Url::fromRoute('entity.booking_status.collection'))->toString(),
      ]),
      '#default_value' => $this->getSetting('statuses'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['target_id'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Booking instance ID'))
      ->setSetting('unsigned', TRUE)
      ->setComputed(TRUE);

    $properties['entity'] = DataReferenceDefinition::create('entity')
      ->setLabel(new TranslatableMarkup('Booking instance'))
      ->setDescription(new TranslatableMarkup('The referenced booking instance'))
      // The entity object is computed out of the entity ID.
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create('booking_instance'))
      ->addConstraint('EntityType', 'booking_instance');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    // This field doesn't need to save any data to db but due to core issues
    // this schema is needed.
    // See https://www.drupal.org/project/drupal/issues/2986836
    // and https://www.drupal.org/project/drupal/issues/2932273.
    return [
      'columns' => [
        'target_id' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Support creation of booking instance entities from provided values.
    if (empty($values['entity'])) {
      if (!empty($values['booking_instance'])) {
        $bookingStorage = \Drupal::service('entity_type.manager')->getStorage('booking_instance');
        if (!empty($values['target_id'])) {
          $values['entity'] = $bookingStorage->load($values['target_id']);
        }
        else {
          $values['entity'] = $bookingStorage->create();
        }
        foreach ($values['entity'] as $field_id => $field) {
          if (isset($values['booking_instance'][$field_id])) {
            $field->setValue($values['booking_instance'][$field_id]);
          }
        }
      }
    }

    // Set a temporary target id on new item creation.
    if (!empty($values['entity']) && !isset($values['target_id'])) {
      $values['target_id'] = 0;
    }

    parent::setValue($values, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $values = parent::getValue();

    if (!empty($values['target_id']) && empty($values['entity'])) {
      $values['entity'] = \Drupal::service('entity_type.manager')->getStorage('booking_instance')->load($values['target_id']);
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    $update_value = FALSE;

    $bookingStorage = \Drupal::service('entity_type.manager')->getStorage('booking_instance');

    // Delete the booking entity.
    if (empty($this->entity) && !empty($this->target_id)) {
      $bookingStorage->load($this->target_id)->delete();
    }

    else {
      if (!$this->entity->id()) {
        $entity = $this->getEntity();
        $this->entity->entity_type_id->set(0, $entity->getEntityTypeId());
        $this->entity->entity_id->set(0, $entity->id());
        $update_value = TRUE;
      }
      try {
        $this->entity->save();
      }
      catch (EntityStorageException $e) {
        $update_value = FALSE;
        if (!empty($previous = $e->getPrevious()) && $previous instanceof BookingApiException) {
          $errors = $previous->renderDetails();
        }
        else {
          $errors = new TranslatableMarkup('unknown');
        }
        $error_msg = new TranslatableMarkup('Cannot save @booking. Errors: @errors', [
          '@booking' => $this->entity->label(),
          '@errors' => $errors,
        ]);
        \Drupal::messenger()->addMessage($error_msg, 'error');
      }
      $this->target_id = $this->entity->id();
    }

    return $update_value;
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    // Remove booking as it'd lead to an non-existing entity.
    if (!empty($this->target_id)) {
      $this->deleteBooking($this->target_id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getPreconfiguredOptions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return (!isset($this->target_id) && empty($this->entity));
  }

  /**
   * Delete booking_instance entity.
   *
   * @param int $id
   *   The booking instance ID.
   */
  protected function deleteBooking($id) {
    $booking_instance = \Drupal::service('entity_type.manager')->getStorage('booking_instance')->load($id);
    if ($booking_instance) {
      $booking_instance->delete();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'target_id';
  }

}
