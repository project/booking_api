<?php

namespace Drupal\booking_api\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\booking_api\Plugin\Field\FieldType\BookingParameters;

/**
 * Represents a configurable entity booking field.
 */
class BookingParametersFieldItemList extends FieldItemList {

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $values = parent::getValue();
    if (!empty($values)) {
      $values = $values[0];
    }

    // Merge in defaults.
    $set_defaults = $this->getFieldDefinition()->getDefaultValueLiteral();
    if (is_array($set_defaults) && isset($set_defaults[0])) {
      $values += $set_defaults[0];
    }
    $values += BookingParameters::DEFAULTS;

    return $values;
  }

}
