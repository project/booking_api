<?php

namespace Drupal\booking_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\SortArray;

/**
 * Plugin implementation of the 'booking_widget' widget.
 *
 * @FieldWidget(
 *   id = "booking_widget",
 *   label = @Translation("Booking widget"),
 *   description = @Translation("Used for editing booking references for this entity."),
 *   field_types = {
 *     "bookings"
 *   }
 * )
 */
class BookingWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Datetime service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The form storage container key.
   *
   * @var string
   */
  protected $storageKey;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeManagerInterface $entityTypeManager,
    AccountInterface $currentUser,
    TimeInterface $time
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings
    );

    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->time = $time;
    $this->storageKey = $field_definition->id() . '_instance_id';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'booking_instance_form_view_mode' => 'default',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $formDisplayStorage = $this->entityTypeManager->getStorage('entity_form_display');
    $form_view_mode_ids = $formDisplayStorage->getQuery()
      ->condition('targetEntityType', 'booking_instance')
      ->execute();
    $form_view_modes = $formDisplayStorage->loadMultiple($form_view_mode_ids);
    $options = [];
    foreach ($form_view_modes as $mode_id => $mode) {
      $options[$mode_id] = empty($mode->label()) ? $mode->getMode() : $mode->label();
    }
    $element['booking_instance_form_view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Booking instance form view mode'),
      '#options' => $options,
      '#default_value' => $this->getSetting('booking_instance_form_view_mode'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Booking instance form view mode: @mode', [
      '@mode' => $this->getSetting('booking_instance_form_view_mode'),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element += [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#element_validate' => [
        [$this, 'validateElement'],
      ],
    ];

    if (!empty($items[$delta]->target_id)) {
      $booking_instance = $this->entityTypeManager->getStorage('booking_instance')->load($items[$delta]->target_id);
    }
    if (empty($booking_instance)) {
      $booking_instance = $this->entityTypeManager->getStorage('booking_instance')->create([
        'user_id' => NULL,
        'from' => $this->time->getRequestTime(),
        'to' => $this->time->getRequestTime(),
        'status' => '',
      ]);
    }

    $element['target_id'] = [
      '#type' => 'value',
      '#value' => $booking_instance->id(),
    ];

    $element['booking_instance'] = [
      '#type' => 'inline_entity_form',
      '#entity_type' => 'booking_instance',
      '#bundle' => 'booking_instance',
      '#default_value' => $booking_instance,
      '#form_mode' => $this->getSetting('booking_instance_form_view_mode'),
      '#save_entity' => FALSE,
      '#op' => 'edit',
    ];

    return $element;
  }

  /**
   * Validate a single booking element.
   */
  public function validateElement($element, FormStateInterface $form_state) {
    $values = $form_state->getValue($element['#parents']);
    $entity_values = $values['booking_instance'];

    // Validate the element itself.
    if (!empty($entity_values['status'][0]['target_id'])) {
      $diff = $entity_values['to'][0]['value']->format('U') - $entity_values['from'][0]['value']->format('U');
      if ($diff <= 0) {
        $form_state->setError($element, $this->t('From date must be earlier than the to date.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    $elements = parent::formMultipleElements($items, $form, $form_state);
    return $elements;
  }

  /**
   * Hepler function to get form display.
   *
   * See https://www.drupal.org/project/drupal/issues/2367933.
   */
  protected function getFormDisplay() {
    $formDisplay = $this->entityTypeManager
      ->getStorage('entity_form_display')
      ->load('booking_instance.booking_instance.default');

    if (!$formDisplay) {
      $formDisplay = $this->entityTypeManager->getStorage('entity_form_display')->create([
        'targetEntityType' => 'booking_instance',
        'bundle' => 'booking_instance',
        'mode' => 'default',
        'hidden' => FALSE,
      ]);
    }
    return $formDisplay;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();

    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name]);
    $key_exists = NULL;
    $values = NestedArray::getValue($form_state->getValues(), $path, $key_exists);

    if ($key_exists) {
      // Remove the 'value' of the 'add more' button.
      unset($values['add_more']);

      // The original delta, before drag-and-drop reordering, is needed to
      // route errors to the correct form element.
      foreach ($values as $delta => &$value) {
        $element_path = $path;
        $element_path[] = 'widget';
        $element_path[] = $delta;
        $element = NestedArray::getValue($form, $element_path);

        if (!empty($value['booking_instance']['status'][0]['target_id'])) {
          $value['_original_delta'] = $delta;
          $value['entity'] = $element['booking_instance']['#entity'];
        }

        // The entity is all we need from this widget.
        unset($value['booking_instance']);
      }

      usort($values, function ($a, $b) {
        return SortArray::sortByKeyInt($a, $b, '_weight');
      });

      // Assign the values and remove the empty ones.
      // Add a flag so setter function knows we're preforming an update.
      $values[0]['_values_update'] = TRUE;
      $items->setValue($values);
      $items->filterEmptyItems();

      // Put delta mapping in $form_state, so that flagErrors() can use it.
      $field_state = static::getWidgetState($form['#parents'], $field_name, $form_state);
      foreach ($items as $delta => $item) {
        $field_state['original_deltas'][$delta] = isset($item->_original_delta) ? $item->_original_delta : $delta;
        unset($item->_original_delta, $item->_weight);
      }
      static::setWidgetState($form['#parents'], $field_name, $form_state, $field_state);
    }
  }

}
