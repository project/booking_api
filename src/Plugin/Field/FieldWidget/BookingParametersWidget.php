<?php

namespace Drupal\booking_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\booking_api\Plugin\Field\FieldType\BookingParameters;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Plugin implementation of the 'booking_parameters_widget' widget.
 *
 * @FieldWidget(
 *   id = "booking_parameters_widget",
 *   label = @Translation("Booking parameters widget"),
 *   description = @Translation("Used to set booking parameters for an entity."),
 *   field_types = {
 *     "booking_parameters"
 *   }
 * )
 */
class BookingParametersWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * Available booking statuses array.
   *
   * @var array
   */
  protected $bookingStatuses;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings
    );

    $this->bookingStatuses = $entityTypeManager->getStorage('booking_status')->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element += [
      '#type' => 'fieldset',
      '#tree' => TRUE,
    ];

    $build_info = $form_state->getBuildInfo();
    if ($build_info['base_form_id'] !== 'field_config_form') {
      $element += [
        '#element_validate' => [
          [$this, 'validateElement'],
        ],
      ];
    }

    $element['enabled'] = [
      '#title' => $this->t('Enable booking'),
      '#description' => $this->t('Should booking be enabled for this entity?'),
      '#type' => 'checkbox',
    ];

    $element['max_bookings'] = [
      '#title' => $this->t('Max bookings'),
      '#description' => $this->t('Maximum total number of bookings for this entity. Set to 0 for unlimited bookings.'),
      '#type' => 'number',
      '#min' => 0,
      '#step' => 1,
    ];

    $element['max_overlapping_bookings'] = [
      '#title' => $this->t('Maximum overlapping bookings'),
      '#description' => $this->t('How many bookings will be possible during the same time period?'),
      '#type' => 'number',
      '#min' => 1,
      '#step' => 1,
    ];

    $element['max_bookings_per_user'] = [
      '#title' => $this->t('Max bookings per user'),
      '#description' => $this->t('Maximum number of bookings a single user can make for this entity. Set to 0 for unlimited bookings per user.'),
      '#type' => 'number',
      '#min' => 0,
      '#step' => 1,
    ];

    $element['start_date'] = [
      '#title' => $this->t('Booking possible from'),
      '#type' => 'datetime',
      '#description' => $this->t('Leave empty for any start date.'),
    ];

    $element['end_date'] = [
      '#title' => $this->t('Booking possible until'),
      '#type' => 'datetime',
      '#description' => $this->t('Leave empty for any end date.'),
    ];

    $element['booking_item_length'] = [
      '#title' => $this->t('Booking item unit'),
      '#description' => $this->t('Single booking will be possible for this amount of time or its multiplications.'),
      '#type' => 'select',
      '#options' => [
        0 => $this->t('From start date to end date (e.g. Booking of the entire event time)'),
        60 => $this->t('A minute'),
        3600 => $this->t('An hour'),
        86400 => $this->t('A day'),
        604800 => $this->t('A week'),
      ],
    ];

    $statuses_options = [];
    foreach ($this->bookingStatuses as $status) {
      $statuses_options[$status->id()] = $status->label();
    }
    $element['default_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Default booking status'),
      '#options' => $statuses_options,
    ];

    // Set default values for elements.
    $set_defaults = $items->getFieldDefinition()->getDefaultValueLiteral();
    $values = $items[$delta]->getValue();
    foreach (BookingParameters::DEFAULTS as $column => $default) {
      if (isset($values[$column])) {
        $element[$column]['#default_value'] = $values[$column];
      }
      elseif (isset($set_defaults[0][$column])) {
        $element[$column]['#default_value'] = $set_defaults[0][$column];
      }
      else {
        $element[$column]['#default_value'] = $default;
      }
    }

    foreach (['start_date', 'end_date'] as $date_column) {
      if ($element[$date_column]['#default_value']) {
        $element[$date_column]['#default_value'] = DrupalDateTime::CreateFromTimestamp($element[$date_column]['#default_value']);
      }
    }
    return $element;
  }

  /**
   * Validate a single field element.
   */
  public function validateElement($element, FormStateInterface $form_state) {
    $values = $form_state->getValue($element['#parents']);

    if ($values['booking_item_length'] == 0) {
      if (empty($values['start_date'])) {
        $form_state->setError($element['start_date'], $this->t('Start date must be given if we allow booking for the entire period only.'));
      }
      if (empty($values['end_date'])) {
        $form_state->setError($element['end_date'], $this->t('End date must be given if we allow booking for the entire period only.'));
      }
    }

    if (!empty($values['start_date']) && !empty($values['end_date'])) {
      if ($values['start_date'] >= $values['end_date']) {
        $form_state->setError($element['start_date'], $this->t('End date must be later than the start date.'));
        $form_state->setError($element['end_date']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values[0]['enabled'] = (bool) $values[0]['enabled'];
    $values[0]['max_bookings'] = intval($values[0]['max_bookings']);
    $values[0]['max_overlapping_bookings'] = intval($values[0]['max_overlapping_bookings']);
    $values[0]['max_bookings_per_user'] = intval($values[0]['max_bookings_per_user']);
    foreach (['start_date', 'end_date'] as $date_column) {
      if ($values[0][$date_column] instanceof DrupalDateTime) {
        $values[0][$date_column] = $values[0][$date_column]->format('U');
      }
      else {
        $values[0][$date_column] = 0;
      }
    }
    $values[0]['booking_item_length'] = intval($values[0]['booking_item_length']);
    return $values;
  }

}
