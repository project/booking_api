<?php

namespace Drupal\booking_api\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Form\FormStateInterface;

/**
 * Represents a configurable entity booking field.
 */
class BookingFieldItemList extends FieldItemList {

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Support passing in only the value of the first item, either as a literal
    // (value of the first property) or as an array of properties.
    if (isset($values) && (!is_array($values) || (!empty($values) && !is_numeric(current(array_keys($values)))))) {
      $values = [0 => $values];
    }

    // Overwrite values completely when loading the field.
    if (empty($values[0]['_values_update'])) {
      if (!isset($values[0]['entity'])) {
        $parent_entity = $this->getEntity();
        if ($parent_entity->id()) {
          $values = [];
          $booking_manager = \Drupal::service('booking_api.booking_manager');
          $bookings = $booking_manager->getBookings($parent_entity, NULL, 0, 0, $this->getSetting('statuses'));
          foreach ($bookings as $id => $booking_entity) {
            $values[] = [
              'target_id' => $id,
              'entity' => $booking_entity,
            ];
          }
        }
      }
    }
    else {
      unset($values[0]['_values_update']);
    }

    parent::setValue($values, $notify);
  }

}
