<?php

namespace Drupal\booking_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\SortArray;

/**
 * Plugin implementation of the 'bookings_table' formatter.
 *
 * @FieldFormatter(
 *   id = "bookings_table",
 *   label = @Translation("Bookings table"),
 *   field_types = {
 *     "bookings"
 *   }
 * )
 */
class BookingsTable extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'booking_instance_view_mode' => 'default',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $viewDisplayStorage = $this->entityTypeManager->getStorage('entity_view_display');
    $view_mode_ids = $viewDisplayStorage->getQuery()
      ->condition('targetEntityType', 'booking_instance')
      ->execute();
    $view_modes = $viewDisplayStorage->loadMultiple($view_mode_ids);
    $options = [];
    foreach ($view_modes as $mode_id => $mode) {
      $options[$mode_id] = empty($mode->label()) ? $mode->getMode() : $mode->label();
    }

    $elements['booking_instance_view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Booking instance view mode'),
      '#description' => $this->t('Will be used to display table fields.'),
      '#options' => $options,
      '#default_value' => $this->getSetting('booking_instance_view_mode'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Booking instance view mode: @mode', [
      '@mode' => $this->getSetting('booking_instance_view_mode'),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );

    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $renderable = [
      '#theme' => 'table',
      '#rows' => [],
      '#header' => [],
      '#empty' => $this->t('There are no bookings.'),
    ];

    // Load booking instances.
    $booking_instance_ids = [];
    foreach ($items as $item) {
      $booking_instance_ids[] = $item->target_id;
    }
    $booking_instances = $this->entityTypeManager->getStorage('booking_instance')->loadMultiple($booking_instance_ids);

    foreach ($booking_instances as $id => $booking_instance) {

      $config_name = $booking_instance->getEntityTypeId()
        . '.' . $booking_instance->bundle()
        . '.' . $this->getSetting('booking_instance_view_mode');
      $viewDisplay = $this->entityTypeManager
        ->getStorage('entity_view_display')
        ->load($config_name);
      $build = $viewDisplay->build($booking_instance);

      uasort($build, function ($a, $b) {
        return SortArray::sortByWeightProperty($a, $b);
      });

      foreach ($build as $field => $field_renderable) {
        if (!isset($renderable['#header'][$field])) {
          if ($fieldDefinition = $booking_instance->getFieldDefinition($field)) {
            $renderable['#header'][$field] = $fieldDefinition->getLabel();
          }
        }
      }

      $renderable['#rows'][$id] = [];
      foreach ($renderable['#header'] as $field => $label) {
        $renderable['#rows'][$id][$field] = [
          'data' => '',
        ];
        if (isset($build[$field])) {
          $renderable['#rows'][$id][$field]['data'] = $build[$field];
        }
      }
    }

    return [0 => $renderable];
  }

}
