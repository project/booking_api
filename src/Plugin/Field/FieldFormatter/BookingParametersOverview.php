<?php

namespace Drupal\booking_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'bookings_table' formatter.
 *
 * @FieldFormatter(
 *   id = "booking_parameters_overview",
 *   label = @Translation("Bookings parameters overview"),
 *   field_types = {
 *     "booking_parameters"
 *   }
 * )
 */
class BookingParametersOverview extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    DateFormatterInterface $dateFormatter,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );

    $this->dateFormatter = $dateFormatter;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('date.formatter'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $parameters = $items->getValue();
    if ($parameters['enabled']) {
      $renderable = [
        '#theme' => 'table',
        '#header' => [
          $this->t('Parameter'),
          $this->t('Value'),
        ],
        '#rows' => [],
      ];

      $renderable['#rows'][] = [
        $this->t('Max bookings'),
        empty($parameters['max_bookings']) ? $this->t('unlimited') : $parameters['max_bookings'],
      ];
      $renderable['#rows'][] = [
        $this->t('Maximum overlapping bookings'),
        ($parameters['max_overlapping_bookings'] > 1) ? $parameters['max_overlapping_bookings'] : $this->t('no overlapping allowed'),
      ];
      $renderable['#rows'][] = [
        $this->t('Max bookings per user'),
        empty($parameters['max_bookings_per_user']) ? $this->t('unlimited') : $parameters['max_bookings'],
      ];
      $renderable['#rows'][] = [
        $this->t('Booking possible from'),
        empty($parameters['start_date']) ? $this->t('no restriction') : $this->dateFormatter->format($parameters['start_date']),
      ];
      $renderable['#rows'][] = [
        $this->t('Booking possible to'),
        empty($parameters['end_date']) ? $this->t('no restriction') : $this->dateFormatter->format($parameters['end_date']),
      ];

      $length_options = [
        0 => $this->t('From start date to end date (e.g. Booking of the entire event time)'),
        60 => $this->t('minute'),
        3600 => $this->t('hour'),
        86400 => $this->t('day'),
        604800 => $this->t('week'),
      ];
      if (isset($length_options[$parameters['booking_item_length']])) {
        $renderable['#rows'][] = [
          $this->t('Booking item unit'),
          $length_options[$parameters['booking_item_length']],
        ];
      }
      else {
        $renderable['#rows'][] = [
          $this->t('Booking item unit'),
          $parameters['booking_item_length'] . ' seconds',
        ];
      }

      $status = $this->entityTypeManager->getStorage('booking_status')->load($parameters['default_status']);
      $renderable['#rows'][] = [
        $this->t('Default (initial) status'),
        $status ? $status->label() : $this->t('not defined'),
      ];
    }
    else {
      $renderable = [
        '#markup' => $this->t('Booking is disabled for this entity.')
      ];
    }

    return [0 => $renderable];
  }

}
