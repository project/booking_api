<?php

namespace Drupal\booking_api;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Booking instance entity.
 *
 * @see \Drupal\booking_api\Entity\BookingInstance.
 */
class BookingInstanceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($account->hasPermission('book for all')) {
      return AccessResult::allowed();
    }
    elseif ($account->hasPermission('make bookings') && $account->id() == $entity->user_id->value) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    if ($account->hasPermission('make bookings') || $account->hasPermission('book for all')) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

}
