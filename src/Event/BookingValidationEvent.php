<?php

namespace Drupal\booking_api\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an entity operation event.
 */
class BookingValidationEvent extends Event {

  /**
   * Booking validation event name.
   */
  const EVENT_NAME = 'booking_validation';

  /**
   * The array of values used to create a new booking instance.
   *
   * @var array
   */
  protected $bookingValues;

  /**
   * The entity that is about to be booked.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $bookedEntity;

  /**
   * Are we creating a new booking instance?
   *
   * @var bool
   */
  protected $isNew;

  /**
   * Validation errors array.
   *
   * @var array
   */
  protected $validationErrors;

  /**
   * Object constructor.
   *
   * @param array $booking_values
   *   The array of values used to create a new booking instance.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that is about to be booked.
   * @param bool $is_new
   *   Are we creating a new booking instance?
   */
  public function __construct(
    array $booking_values,
    EntityInterface $entity,
    $is_new
  ) {
    $this->bookingValues = $booking_values;
    $this->bookedEntity = $entity;
    $this->isNew = $is_new;
    $this->validationErrors = [];
  }

  /**
   * Gets booking values array.
   *
   * @return array
   *   The booking values.
   */
  public function getBookingValues() {
    return $this->bookingValues;
  }

  /**
   * Gets the entity about to be booked.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getBookedEntity() {
    return $this->bookedEntity;
  }

  /**
   * Is the booking new.
   *
   * @return bool
   *   Is the booking new?
   */
  public function isNewBooking() {
    return $this->isNew;
  }

  /**
   * Add validation errors.
   *
   * @param array $errors
   *   Array of errors to add.
   */
  public function addValidationErrors(array $errors) {
    $this->validationErrors += $errors;
  }

  /**
   * Get validation errors.
   *
   * @return array
   *   Validation errors array.
   */
  public function getValidationErrors() {
    return $this->validationErrors;
  }

}
