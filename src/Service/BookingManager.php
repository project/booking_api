<?php

namespace Drupal\booking_api\Service;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\booking_api\Event\BookingValidationEvent;
use Drupal\user\UserInterface;
use Drupal\booking_api\Entity\BookingInstanceInterface;
use Drupal\booking_api\Exception\BookingApiException;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Defines the Booking Manager service.
 */
class BookingManager implements BookingManagerInterface {

  use StringTranslationTrait;

  /**
   * Max number of query results.
   */
  const MAX_QUERY_RESULTS = 200;

  /**
   * Booking storage.
   *
   * @var \Drupal\Core\Entity\Sql\SqlEntityStorageInterface
   */
  protected $bookingStorage;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Available booking statuses array.
   *
   * @var array
   */
  protected $bookingStatuses;

  /**
   * The current user object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Drupal cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user instance.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Drupal cache.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    AccountInterface $currentUser,
    CacheBackendInterface $cache,
    EventDispatcherInterface $event_dispatcher
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->bookingStorage = $entityTypeManager->getStorage('booking_instance');
    $this->bookingStatuses = $entityTypeManager->getStorage('booking_status')->loadMultiple();
    $this->currentUser = $currentUser;
    $this->cache = $cache;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatuses($as_options = TRUE) {
    if ($as_options) {
      $options = [];
      foreach ($this->bookingStatuses as $id => $status) {
        $options[$id] = $status->label();
      }
      return $options;
    }
    return $this->bookingStatuses;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsField(EntityInterface $entity) {
    $cid = $entity->getEntityTypeId() . '.' . $entity->bundle() . '.booking_settings_field';
    if ($cached = $this->cache->get($cid)) {
      $settings_field = $cached->data;
    }
    else {
      $settings_field = '';
      foreach ($entity->getFieldDefinitions() as $name => $definition) {
        if ($definition instanceof FieldConfigInterface && $definition->getType() === 'booking_parameters') {
          $settings_field = $name;
          break;
        }
      }
      $this->cache->set($cid, $settings_field, CacheBackendInterface::CACHE_PERMANENT, [
        $entity->getEntityType()->getBundleEntityType() . ':' . $entity->bundle(),
      ]);
    }

    return $settings_field;
  }

  /**
   * {@inheritdoc}
   */
  public function validateBooking(array &$booking_values, EntityInterface $bookedEntity, $is_new = TRUE) {
    $errors = [];

    // Map field structure values to flat structure for easier validation.
    foreach (['from', 'to', 'entity_type_id', 'entity_id', 'user_id', 'status'] as $field) {
      if (isset($booking_values[$field]) && is_array($booking_values[$field])) {
        $column = 'value';
        if (in_array($field, ['user_id', 'status'], TRUE)) {
          $column = 'target_id';
        }
        $booking_values[$field] = $booking_values[$field][0][$column];
      }
    }

    if (!$settings_field = $this->getSettingsField($bookedEntity)) {
      $errors[10] = $this->t('No booking settings for the booked entity.');
    }
    else {
      $settings = $bookedEntity->{$settings_field}->getValue();
      if (!$settings['enabled']) {
        $errors[11] = $this->t('Booking is disabled for this entity.');
      }
      else {
        // Add defaults.
        if ($is_new) {
          $booking_values += [
            'entity_type_id' => $bookedEntity->getEntityTypeId(),
            'entity_id' => $bookedEntity->id(),
            'user_id' => $this->currentUser->id(),
            'status' => $settings['default_status'],
          ];
        }

        // Convert dates to timestamp.
        foreach (['from', 'to'] as $date_column) {
          if (!empty($booking_values[$date_column]) && $booking_values[$date_column] instanceof DrupalDateTime) {
            $booking_values[$date_column] = $booking_values[$date_column]->format('U');
          }
        }
        if ($settings['booking_item_length']) {
          if (empty($booking_values['from']) || empty($booking_values['to'])) {
            $errors[12] = $this->t('Both from and to dates of the booking must be provided.');
          }
          elseif ($booking_values['to'] - $booking_values['from'] !== (int) $settings['booking_item_length']) {
            $errors[13] = $this->t('Booking period is different than the set value.');
          }
          if ($booking_values['from'] >= $booking_values['to']) {
            $errors[14] = $this->t('From date must be earlier than the to date.');
          }
        }
        else {
          $booking_values['from'] = $settings['start_date'];
          $booking_values['to'] = $settings['end_date'];
        }

        // Check limits for new bookings.
        if ($is_new) {
          // Max total bookings.
          if ($settings['max_bookings']) {
            $query = $this->bookingStorage->getQuery();
            $query
              ->condition('entity_type_id', $bookedEntity->getEntityTypeId())
              ->condition('entity_id', $bookedEntity->id());
            $count = $query->count()->execute();
            if ($count >= $settings['max_bookings']) {
              $errors[15] = $this->t('No more bookings allowed for this entity.');
            }
          }

          // Max overlapping bookings.
          if ($settings['max_overlapping_bookings']) {
            $query = $this->bookingStorage->getQuery();
            $query
              ->condition('entity_type_id', $bookedEntity->getEntityTypeId())
              ->condition('entity_id', $bookedEntity->id());
            $or = $query->orConditionGroup();
            $or->condition($query->andConditionGroup()
              ->condition('from', $booking_values['from'], '<=')
              ->condition('to', $booking_values['to'], '>')
            );
            $or->condition($query->andConditionGroup()
              ->condition('from', $booking_values['to'], '<')
              ->condition('to', $booking_values['to'], '>=')
            );
            $query->condition($or);
            $count = $query->count()->execute();
            if ($count >= $settings['max_overlapping_bookings']) {
              $errors[16] = $this->t('No more bookings allowed within the given time period.');
            }
          }

          // Maximum bookings per user.
          if ($settings['max_bookings_per_user']) {
            $query = $this->bookingStorage->getQuery();
            $query
              ->condition('entity_type_id', $bookedEntity->getEntityTypeId())
              ->condition('entity_id', $bookedEntity->id())
              ->condition('user_id', $booking_values['user_id']);
            $count = $query->count()->execute();
            if ($count >= $settings['max_bookings_per_user']) {
              $errors[17] = $this->t('No more bookings allowed for this user.');
            }
          }
        }

        // Status check.
        if (!array_key_exists($booking_values['status'], $this->bookingStatuses)) {
          $errors[18] = $this->t('Trying to assign an non-existing status to a booking.');
        }
      }
    }

    // Allow other modules to take part in validating.
    $validationEvent = new BookingValidationEvent($booking_values, $bookedEntity, $is_new);
    $validationEvent->addValidationErrors($errors);
    $this->eventDispatcher->dispatch($validationEvent::EVENT_NAME, $validationEvent);

    return $validationEvent->getValidationErrors();
  }

  /**
   * {@inheritdoc}
   */
  public function createBooking(array $values = []) {
    $booking = $this->bookingStorage->create($values);
    return $booking;
  }

  /**
   * {@inheritdoc}
   */
  public function getBookingsByQuery(array $conditions, array $pager_data = [], $load_entities = FALSE) {
    $query = $this->bookingStorage->getQuery();
    foreach ($conditions as $key => $param) {
      if (substr($key, 0, 1) === '_') {
        continue;
      }
      if (count($param) === 2) {
        $query->condition($param[0], $param[1]);
      }
      else {
        if (count($param) !== 3) {
          throw new BookingApiException('Invalid query parameters.');
        }
        $query->condition($param[0], $param[1], $param[2]);
      }
    }

    $result = [
      'total' => (clone $query)->count()->execute(),
      'results' => [],
    ];

    $this->setPager($query, $pager_data);

    $result['results'] += $query->execute();

    if ($load_entities) {
      $result['results'] = $this->bookingStorage->loadMultiple($result['results']);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getBookings(EntityInterface $entity, $user_id = NULL, $from = 0, $to = 0, array $statuses = [], array $pager_data = []) {
    $query = $this->bookingStorage->getQuery()
      ->condition('entity_type_id', $entity->getEntityTypeId(), '=')
      ->condition('entity_id', $entity->id(), '=');
    if ($user_id) {
      $query->condition('user_id', $user_id);
    }
    if ($from) {
      $query->condition('from', $from, '>');
    }
    if ($to) {
      $query->condition('to', $to, '<');
    }
    if (!empty($statuses)) {
      $query->condition('status', $statuses, 'IN');
    }

    $this->setPager($query, $pager_data);

    $booking_ids = $query->execute();

    return $this->bookingStorage->loadMultiple($booking_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getBookedEntities(array $bookings) {
    $entity_data = [];
    foreach ($bookings as $booking) {
      $entity_type_id = $booking->entity_type_id->first()->getValue()['value'];
      $entity_id = $booking->entity_id->first()->getValue()['value'];
      $entity_data[$entity_type_id][$entity_id] = $entity_id;
    }

    $output = [];
    foreach ($entity_data as $entity_type_id => $entity_ids) {
      $output[$entity_type_id] = $this->entityTypeManager->getStorage($entity_type_id)->loadMultiple($entity_ids);
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getBookedEntity(BookingInstanceInterface $booking) {
    $entity_type_id = $booking->entity_type_id->first()->getValue()['value'];
    $result = $this->getBookedEntities([$booking]);
    if (!empty($result[$entity_type_id])) {
      return reset($result[$entity_type_id]);
    }
  }

  /**
   * Helper function to set up pager for query.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query to apply pager data to.
   * @param array $pager_data
   *   Array containing pager offset and limit.
   */
  protected function setPager(QueryInterface $query, array $pager_data = []) {
    $pager_data += [
      'offset' => 0,
      'limit' => 50,
    ];
    if ($pager_data['limit'] > static::MAX_QUERY_RESULTS) {
      $pager_data['limit'] = static::MAX_QUERY_RESULTS;
    }
    $query->range($pager_data['offset'], $pager_data['limit']);
  }

  /**
   * {@inheritdoc}
   */
  public function loadBooking($id) {
    return $this->bookingStorage->load($id);
  }

}
