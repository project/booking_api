<?php

namespace Drupal\booking_api\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\user\UserInterface;
use Drupal\booking_api\Entity\BookingInstanceInterface;

/**
 * Defines the Booking Manager service interface.
 */
interface BookingManagerInterface {

  /**
   * Get booking statuses config entities.
   *
   * @param bool $as_options
   *   Should the function return options for a form element?
   *
   * @return array
   *   Array of booking_status config entities
   */
  public function getStatuses($as_options = TRUE);

  /**
   * Get settings field for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The settings field instance name.
   */
  public function getSettingsField(EntityInterface $entity);

  /**
   * Validate booking values.
   *
   * @param array $booking_values
   *   Values of the booking entity - flat or with a field structure.
   * @param \Drupal\Core\Entity\EntityInterface $bookedEntity
   *   The entity that is being booked.
   * @param bool $is_new
   *   Is the booking new or is it an edit?
   *
   * @return array
   *   Array of error messages keyed by error codes.
   */
  public function validateBooking(array &$booking_values, EntityInterface $bookedEntity, $is_new = TRUE);

  /**
   * Create a new booking_instance entity.
   *
   * @param array $values
   *   Entity values array.
   *
   * @return \Drupal\booking_api\Entity\BookingInstanceInterface
   *   A new booking_instance entity.
   */
  public function createBooking(array $values = []);

  /**
   * Get booking ids by entering entity query conditions array.
   *
   * @param array $conditions
   *   Array of conditions as used by entity query condition method.
   * @param array $pager_data
   *   Pager data array containing offset and limit parameters.
   * @param bool $load_entities
   *   Should output contain loaded booking_instance entities or just their IDs?
   *
   * @return array
   *   Array containing total count and results (booking entities or IDs only).
   */
  public function getBookingsByQuery(array $conditions, array $pager_data = [], $load_entities = FALSE);

  /**
   * Get bookings for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   * @param int $user_id
   *   The ID of the user for which bookings should be retrieved.
   *   Leave empty for bookings of all users for the given entity.
   * @param int $from
   *   The timestamp from which to check.
   * @param int $to
   *   The timestamp to which to check.
   * @param array $statuses
   *   Array of booking statuses to get.
   * @param array $pager_data
   *   Pager data array containing offset and limit parameters.
   *
   * @return array
   *   Array of booking entities.
   */
  public function getBookings(EntityInterface $entity, $user_id = NULL, $from = 0, $to = 0, array $statuses = [], array $pager_data = []);

  /**
   * Get list of booked entities for given bookings.
   *
   * @param array $bookings
   *   Array of booking_instance entities.
   *
   * @return array
   *   Array of booked entities keyed by entity type ID and entity ID.
   */
  public function getBookedEntities(array $bookings);

  /**
   * Get booked entity for given booking.
   *
   * @param \Drupal\booking_api\Entity\BookingInstanceInterface $booking
   *   Array of booking_instance entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of booked entities keyed by entity type ID and entity ID.
   */
  public function getBookedEntity(BookingInstanceInterface $booking);

  /**
   * Load a single booking by ID.
   *
   * @param int $id
   *   The booking ID.
   */
  public function loadBooking($id);

}
