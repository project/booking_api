<?php

namespace Drupal\booking_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\booking_api\Service\BookingManagerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Link;

/**
 * Booking API controller class.
 */
class BookingApiController extends ControllerBase {

  use RedirectDestinationTrait;

  /**
   * The booking manager service.
   *
   * @var \Drupal\booking_api\Service\BookingManagerInterface
   */
  protected $bookingManager;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('booking_api.booking_manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * Constructs a new BookingApiController object.
   *
   * @param \Drupal\booking_api\Service\BookingManagerInterface $bookingManager
   *   The booking manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   */
  public function __construct(
    BookingManagerInterface $bookingManager,
    DateFormatterInterface $dateFormatter
  ) {
    $this->bookingManager = $bookingManager;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * Page callback for user bookings tab.
   */
  public function userBookings(UserInterface $user) {
    $conditions = [
      ['user_id', $user->id()],
    ];
    $bookings = $this->bookingManager->getBookingsByQuery($conditions, [], TRUE);

    $table = [
      '#theme' => 'table',
      '#empty' => $this->t('You have no bookings.'),
      '#header' => [
        'booked' => $this->t('Booked entity'),
        'created' => $this->t('Created'),
        'from' => $this->t('Booked from'),
        'to' => $this->t('Booked to'),
        'status' => $this->t('Status'),
        'operations' => $this->t('Operations'),
      ],
      '#rows' => [],
    ];

    if (!empty($bookings['results'])) {

      $booked_entities = $this->bookingManager->getBookedEntities($bookings['results']);
      $statuses = $this->bookingManager->getStatuses();
      foreach ($bookings['results'] as $booking) {

        $row = [];

        $entity_type_id = $booking->entity_type_id[0]->getValue()['value'];
        $entity_id = $booking->entity_id[0]->getValue()['value'];
        $row['booked'] = $booked_entities[$entity_type_id][$entity_id]->toLink()->toString();

        $row['created'] = $this->dateFormatter->format($booking->created[0]->getValue()['value']);

        $row['from'] = $this->dateFormatter->format($booking->from[0]->getValue()['value']);

        $row['to'] = $this->dateFormatter->format($booking->to[0]->getValue()['value']);

        $status = $booking->status[0]->getValue()['target_id'];
        $row['status'] = isset($statuses[$status]) ? $statuses[$status] : $this->t('non-existing status');

        $destination_options = ['query' => $this->getRedirectDestination()->getAsArray()];
        $row['operations']['data'] = [
          '#theme' => 'item_list',
          '#items' => [
            Link::createFromRoute(
              $this->t('Edit'),
              'entity.booking_instance.edit_form',
              ['booking_instance' => $booking->id()],
              $destination_options
            )->toString(),
            Link::createFromRoute(
              $this->t('Delete'),
              'entity.booking_instance.delete_form',
              ['booking_instance' => $booking->id()],
              $destination_options
            )->toString(),
          ],
        ];

        $table['#rows'][] = $row;
      }
    }

    return $table;
  }

}
