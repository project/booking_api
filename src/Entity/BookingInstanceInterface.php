<?php

namespace Drupal\booking_api\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Booking instance entities.
 *
 * @ingroup booking_api
 */
interface BookingInstanceInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Booking instance name.
   *
   * @return string
   *   Name of the Booking instance.
   */
  public function getName();

  /**
   * Sets the Booking instance name.
   *
   * @param string $name
   *   The Booking instance name.
   *
   * @return \Drupal\booking_api\Entity\BookingInstanceInterface
   *   The called Booking instance entity.
   */
  public function setName($name);

  /**
   * Gets the Booking instance creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Booking instance.
   */
  public function getCreatedTime();

  /**
   * Sets the Booking instance creation timestamp.
   *
   * @param int $timestamp
   *   The Booking instance creation timestamp.
   *
   * @return \Drupal\booking_api\Entity\BookingInstanceInterface
   *   The called Booking instance entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Booking instance published status indicator.
   *
   * Unpublished Booking instance are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Booking instance is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Booking instance.
   *
   * @param bool $published
   *   TRUE to set this Booking instance to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\booking_api\Entity\BookingInstanceInterface
   *   The called Booking instance entity.
   */
  public function setPublished($published);

  /**
   * Get the status ID of the booking.
   *
   * @return string
   *   The status ID.
   */
  public function getStatus();

  /**
   * Set status of the booking.
   *
   * @param string $status
   *   The status ID.
   *
   * @return \Drupal\booking_api\Entity\BookingInstanceInterface
   *   The called Booking instance entity.
   */
  public function setStatus($status);

}
