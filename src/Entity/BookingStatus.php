<?php

namespace Drupal\booking_api\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Booking status entity.
 *
 * @ConfigEntityType(
 *   id = "booking_status",
 *   label = @Translation("Booking status"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\booking_api\BookingStatusListBuilder",
 *     "form" = {
 *       "add" = "Drupal\booking_api\Form\BookingStatusForm",
 *       "edit" = "Drupal\booking_api\Form\BookingStatusForm",
 *       "delete" = "Drupal\booking_api\Form\BookingStatusDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\booking_api\BookingStatusHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "booking_status",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/booking_status/{booking_status}",
 *     "add-form" = "/admin/structure/booking_status/add",
 *     "edit-form" = "/admin/structure/booking_status/{booking_status}/edit",
 *     "delete-form" = "/admin/structure/booking_status/{booking_status}/delete",
 *     "collection" = "/admin/structure/booking_status"
 *   },
 *   config_export = {
 *    "id",
 *    "label",
 *    "uuid",
 *    "langcode",
 *    "status",
 *    "dependencies"
 * }
 * )
 */
class BookingStatus extends ConfigEntityBase implements BookingStatusInterface {

  /**
   * The Booking status ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Booking status label.
   *
   * @var string
   */
  protected $label;

}
