<?php

namespace Drupal\booking_api\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Booking instance entities.
 */
class BookingInstanceViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Update booking owner relationship label.
    $data['booking_instance']['user_id']['relationship']['title'] = $this->t('Booking owner (user)');
    $data['booking_instance']['user_id']['relationship']['label'] = $this->t('Booking owner (user)');

    $data['booking_instance']['booked_entity'] = [
      'title' => $this->t('Booked entity label'),
      'help' => $this->t('The label of the booked entity.'),
      'id' => 'booking_api_booked_entity',
      'field' => [
        'id' => 'booking_api_booked_entity_field',
      ],
      'real field' => 'entity_id',
    ];

    $data['booking_instance']['booking_status'] = [
      'title' => $this->t('Booking Status'),
      'help' => $this->t('The booking status with no access check.'),
      'id' => 'booking_api_booking_status',
      'field' => [
        'id' => 'booking_api_booking_status',
      ],
      'real field' => 'status',
    ];
    
    return $data;
  }

}
