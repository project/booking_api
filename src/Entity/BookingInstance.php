<?php

namespace Drupal\booking_api\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\booking_api\Exception\BookingApiException;

/**
 * Defines the Booking instance entity.
 *
 * @ingroup booking_api
 *
 * @ContentEntityType(
 *   id = "booking_instance",
 *   label = @Translation("Booking instance"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\booking_api\Entity\BookingInstanceViewsData",
 *     "storage_schema" = "Drupal\booking_api\BookingInstanceStorageSchema",
 *     "access" = "Drupal\booking_api\BookingInstanceAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\booking_api\BookingInstanceHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "edit" = "Drupal\booking_api\Form\BookingForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *   },
 *   base_table = "booking_instance",
 *   data_table = "booking_instance_field_data",
 *   admin_permission = "administer booking instance entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "status" = "status",
 *     "entity_id" = "entity_id",
 *   },
 *   links = {
 *     "canonical" = "/booking/{booking_instance}",
 *     "edit-form" = "/admin/content/booking/{booking_instance}/edit",
 *     "delete-form" = "/admin/content/booking/{booking_instance}/delete",
 *   },
 *   field_ui_base_route = "booking_instance.settings"
 * )
 */
class BookingInstance extends ContentEntityBase implements BookingInstanceInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    // Add access check.
    if ($this->id()) {
      $operation = 'update';
    }
    else {
      $operation = 'create';
    }
    
    if( PHP_SAPI !== 'cli'){
      // Temp solution, $accessResult is returning null!.
      $current_user = \Drupal::currentUser();
      if (!$current_user->hasPermission('book for all') && !$current_user->hasPermission('make bookings')) {
        throw new BookingApiException(sprintf('Booking for %s cannot be performed: access denied.', $this->user_id[0]->entity->label()));
      }
    }

    // This for handle the migration process, should be removed when the migration is over.
    /* if (\Drupal::currentUser()->id() != 0) {
      $accessResult = $this->access($operation, $this->user_id[0]->entity, TRUE);

      if ($accessResult->isForbidden()) {
        throw new BookingApiException(sprintf('Booking for %s cannot be performed: access denied.', $this->user_id[0]->entity->label()), $accessResult->getReason());
      }  
    }*/
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return sprintf('booking for user %s', $this->user_id[0]->entity->label());
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->entity->id();
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Booked by'))
      ->setDescription(t('The ID of the user that owns the booking.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', [
        'include_anonymous' => FALSE,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'label' => 'inline',
        'type' => 'entity_reference_autocomplete',
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['entity_type_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Booked entity type ID'))
      ->setDescription(t('The ID of the entity type this booking is for.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH)
      ->setRevisionable(TRUE);

    $fields['entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Booked entity ID'))
      ->setDescription(t('The ID of the  entity this booking is for.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE);

    $fields['from'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Booked from'))
      ->setDescription(t('The time that the target entity is booked from.'))
      ->setDefaultValue(NULL)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'label' => 'inline',
        'type' => 'datetime_timestamp',
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['to'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Booked to'))
      ->setDescription(t('The time that the target entity is booked to.'))
      ->setDefaultValue(NULL)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'label' => 'inline',
        'type' => 'datetime_timestamp',
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Booking status'))
      ->setDescription(t('The ID of the current booking status.'))
      ->setSetting('target_type', 'booking_status')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'label' => 'inline',
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the booking entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the booking entity was changed.'));

    return $fields;
  }

}
