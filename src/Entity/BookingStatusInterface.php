<?php

namespace Drupal\booking_api\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Booking status entities.
 */
interface BookingStatusInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
