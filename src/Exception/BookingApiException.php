<?php

namespace Drupal\booking_api\Exception;

use Exception;

/**
 * Defines Booking API exception.
 */
class BookingApiException extends Exception {

  /**
   * Exception details stored as JSON.
   *
   * @var string
   */
  protected $details;

  /**
   * Constructor.
   *
   * @param string $message
   *   Error message.
   * @param string $details
   *   Error details.
   */
  public function __construct($message, $details = '') {
    parent::__construct($message);
    $this->details = $details;
  }

  /**
   * Get exception details.
   */
  public function getDetails() {
    return $this->details;
  }

  /**
   * Get details in a readable string.
   *
   * @param bool $errors_only
   *   Should only errors be included in the output?
   */
  public function renderDetails($errors_only = TRUE) {
    $output = [];
    if ($data = json_decode($this->details, TRUE)) {
      foreach ($data as $item) {
        if ($errors_only && $item['result']) {
          continue;
        }
        $output[] = $item['description'];
      }
    }
    return implode(', ', $output);
  }

}
