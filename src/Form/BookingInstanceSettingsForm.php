<?php

namespace Drupal\booking_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BookingInstanceSettingsForm.
 *
 * @ingroup booking_api
 */
class BookingInstanceSettingsForm extends ConfigFormBase {

  const CONFIG_NAME = 'booking_api.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'bookinginstance_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);

    $form_state->cleanValues();
    $values = $form_state->getValues();

    foreach ($values as $name => $value) {
      $config->set($name, $value);
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Defines the settings form for Booking instance entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);
    $form['expiration_period'] = [
      '#type' => 'number',
      '#step' => 1,
      '#min' => 0,
      '#title' => $this->t('Booking expiration period'),
      '#description' => $this->t('Bookings that have ending time more that number of hours will be deleted. Set to zero to keep booking history forever.'),
      '#default_value' => $config->get('expiration_period'),
    ];

    return parent::buildForm($form, $form_state);
  }

}
