<?php

namespace Drupal\booking_api\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;

/**
 * Node booking tab form class.
 */
class NodeBookingTabForm extends EntityBookingForm {

  const ENTITY_TYPE = 'node';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'booking_api_node_tab_form';
  }

  /**
   * The form builder function.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\node\NodeInterface $node
   *   The account for which points are edited.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    return parent::buildFormCommon($form, $form_state, $node);
  }

}
