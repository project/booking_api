<?php

namespace Drupal\booking_api\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\booking_api\Service\BookingManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;

/**
 * Form controller for IKO Skill edit forms.
 *
 * @ingroup booking_api
 */
class BookingForm extends ContentEntityForm {

  /**
   * The booking manager service.
   *
   * @var \Drupal\booking_api\Service\BookingManagerInterface
   */
  protected $bookingManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\booking_api\Service\BookingManagerInterface $bookingManager
   *   The booking manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user instance.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    TimeInterface $time = NULL,
    BookingManagerInterface $bookingManager,
    EntityTypeManagerInterface $entityTypeManager,
    AccountInterface $currentUser
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->bookingManager = $bookingManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('booking_api.booking_manager'),
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $booked_entity = $this->entityTypeManager->getStorage($this->entity->entity_type_id[0]->getValue()['value'])->load($this->entity->entity_id[0]->getValue()['value']);
    $form['booked_entity'] = [
      '#theme' => 'item_list',
      '#items' => [
        $this->t('Booked entity type ID: @id', ['@id' => $booked_entity->getEntityTypeId()]),
        $this->t('Booked entity: @link', ['@link' => $booked_entity->toLink()->toString()]),
      ],
    ];

    $params_field = $this->bookingManager->getSettingsField($booked_entity);
    if ($params_field && !empty($booked_entity->{$params_field})) {
      $parameters = $booked_entity->{$params_field}->getValue();
    }
    if (!isset($parameters) || !$parameters['enabled']) {
      $this->messenger()->addError($this->t('Booking is disabled for this entity.'));
      return $form;
    }

    $form_state->set('booked_entity', $booked_entity);

    $form = parent::buildForm($form, $form_state);

    if (!$parameters['booking_item_length']) {
      unset($form['from']);
      unset($form['to']);
    }
    // Allow normal users only to set the default status.
    if (!$this->currentUser->hasPermission('book for all')) {
      unset($form['status']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $form_state->cleanValues();
    $entity_values = $form_state->getValues();
    $errors = $this->bookingManager->validateBooking($entity_values, $form_state->get('booked_entity'), FALSE);
    if (!empty($errors)) {
      foreach ($errors as $error) {
        $form_state->setError($form, $error);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $this->messenger()->addStatus('Booking updated.');
  }

}
