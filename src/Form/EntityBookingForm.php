<?php

namespace Drupal\booking_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\booking_api\Service\BookingManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Link;

/**
 * Entity booking form base class.
 */
abstract class EntityBookingForm extends FormBase {

  use RedirectDestinationTrait;

  /**
   * The booking manager service.
   *
   * @var \Drupal\booking_api\Service\BookingManagerInterface
   */
  protected $bookingManager;

  /**
   * The current user object.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $currentUser;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new UserpointsUserEditTabForm object.
   *
   * @param \Drupal\booking_api\Service\BookingManagerInterface $bookingManager
   *   The booking manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user instance.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   */
  public function __construct(
    BookingManagerInterface $bookingManager,
    EntityTypeManagerInterface $entityTypeManager,
    AccountInterface $currentUser,
    DateFormatterInterface $dateFormatter
  ) {
    $this->bookingManager = $bookingManager;
    $this->currentUser = $entityTypeManager->getStorage('user')->load($currentUser->id());
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('booking_api.booking_manager'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'booking_api_entity_booking_form';
  }

  /**
   * The form builder function.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity for which bookings are managed.
   *
   * @return array
   *   The form structure.
   */
  public function buildFormCommon(array $form, FormStateInterface $form_state, ContentEntityInterface $entity = NULL) {

    $params_field = $this->bookingManager->getSettingsField($entity);
    if ($params_field && !empty($entity->{$params_field})) {
      $parameters = $entity->{$params_field}->getValue();
    }
    if (!isset($parameters) || !$parameters['enabled']) {
      return [
        '#markup' => $this->t('Booking is disabled for this entity.'),
      ];
    }

    $form_state->set('booked_entity', $entity);

    $form['booking'] = [
      '#type' => 'details',
      '#title' => $this->t('New booking'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];
    $form['booking']['booking_instance'] = [
      '#parents' => ['booking', 'booking_instance'],
      '#tree' => TRUE,
    ];
    $booking = $this->bookingManager->createBooking(['status' => $parameters['default_status']]);
    $booking_form_display = EntityFormDisplay::collectRenderDisplay($booking, 'default');
    $booking_form_display->buildForm($booking, $form['booking']['booking_instance'], $form_state);

    if (!$parameters['booking_item_length']) {
      unset($form['booking']['booking_instance']['from']);
      unset($form['booking']['booking_instance']['to']);
    }
    if (!$this->currentUser->hasPermission('book for all')) {
      unset($form['booking']['booking_instance']['user_id']);
      unset($form['booking']['booking_instance']['status']);
    }

    $form['booking']['make_booking'] = [
      '#type' => 'submit',
      '#value' => $this->t('Book'),
    ];

    $form['bookings'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#tree' => TRUE,
      '#title' => $this->t('Bookings'),
    ];

    $bookings_table_html_id = strtr($entity->getEntityTypeId(), ['_' => '-']) . '-bookings-table';

    // Filters.
    if ($this->currentUser->hasPermission('book for all') || $parameters['booking_item_length']) {
      $form['bookings']['filters'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Filters'),
        '#tree' => TRUE,
      ];
      if ($this->currentUser->hasPermission('book for all')) {
        $form['bookings']['filters'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Filters'),
          '#tree' => TRUE,
        ];
        $form['bookings']['filters']['user_id'] = [
          '#title' => $this->t('User'),
          '#type' => 'entity_autocomplete',
          '#target_type' => 'user',
          '#selection_handler' => 'default',
          '#selection_settings' => [
            'include_anonymous' => FALSE,
          ],
          '#default_value' => $this->currentUser,
        ];

        $form['bookings']['filters']['statuses'] = [
          '#title' => $this->t('Status'),
          '#type' => 'select',
          '#multiple' => TRUE,
          '#options' => $this->bookingManager->getStatuses(),
        ];
      }
      if ($parameters['booking_item_length']) {
        $form['bookings']['filters']['from'] = [
          '#title' => $this->t('Booked from'),
          '#type' => 'datetime',
          '#description' => $this->t('Leave empty for any start date.'),
        ];

        $form['bookings']['filters']['to'] = [
          '#title' => $this->t('Booked to'),
          '#type' => 'datetime',
          '#description' => $this->t('Leave empty for any end date.'),
        ];
      }

      $form['bookings']['filters']['apply_filters'] = [
        '#type' => 'submit',
        '#value' => $this->t('Apply filters'),
        '#ajax' => [
          'callback' => [get_called_class(), 'ajaxForm'],
          'wrapper' => $bookings_table_html_id,
        ],
      ];
    }

    // Display booking information.
    $filters = $this->getFilterValues($form_state);
    $form['bookings']['table'] = $this->getBookingsTable($entity, $filters, $parameters);
    $form['bookings']['table']['#attributes']['id'] = $bookings_table_html_id;

    return $form;
  }

  /**
   * Ajax callback for this form.
   */
  public static function ajaxForm(array $form, FormStateInterface $form_state) {
    return $form['bookings']['table'];
  }

  /**
   * Gets filter values.
   */
  protected function getFilterValues(FormStateInterface $form_state) {
    $filters = $form_state->getValue(['bookings', 'filters'], []);
    $filters += [
      'user_id' => $this->currentUser->id(),
      'from' => 0,
      'to' => 0,
      'statuses' => [],
    ];
    return $filters;
  }

  /**
   * Gets the bookings table.
   */
  protected function getBookingsTable(ContentEntityInterface $entity, array $filters, array $parameters) {
    $table = [
      '#type' => 'table',
      '#empty' => $this->t('There are no bookings that meet the given criteria.'),
      '#header' => [
        'created' => $this->t('Created'),
        'user' => $this->t('User'),
        'from' => $this->t('Booked from'),
        'to' => $this->t('Booked to'),
        'status' => $this->t('Status'),
        'operations' => $this->t('Operations'),
      ],
      '#rows' => [],
    ];

    $bookings = $this->bookingManager->getBookings($entity, $filters['user_id'], $filters['from'], $filters['to'], $filters['statuses']);
    $statuses = $this->bookingManager->getStatuses();

    foreach ($bookings as $id => $booking) {
      $table[$id]['created'] = [
        '#markup' => $this->dateFormatter->format($booking->created[0]->getValue()['value']),
      ];
      $table[$id]['user'] = [
        '#markup' => $booking->user_id[0]->entity->toLink()->toString(),
      ];
      $table[$id]['from'] = [
        '#markup' => $this->dateFormatter->format($booking->from[0]->getValue()['value']),
      ];
      $table[$id]['to'] = [
        '#markup' => isset($booking->to[0]) ? $this->dateFormatter->format($booking->to[0]->getValue()['value']) : NULL,
      ];

      $status = $booking->status[0]->getValue()['target_id'];
      $table[$id]['status'] = [
        '#markup' => isset($statuses[$status]) ? $statuses[$status] : $this->t('non-existing status'),
      ];

      $destination_options = ['query' => $this->getRedirectDestination()->getAsArray()];
      $table[$id]['operations'] = [
        '#theme' => 'item_list',
        '#items' => [
          Link::createFromRoute(
            $this->t('Edit'),
            'entity.booking_instance.edit_form',
            ['booking_instance' => $booking->id()],
            $destination_options
          )->toString(),
          Link::createFromRoute(
            $this->t('Delete'),
            'entity.booking_instance.delete_form',
            ['booking_instance' => $booking->id()],
            $destination_options
          )->toString(),
        ],
      ];
    }

    return $table;
  }

  /**
   * Gets the current operation.
   */
  protected function getOp(FormStateInterface $form_state) {
    static $op;
    if (!isset($op)) {
      $trigger = $form_state->getTriggeringElement();
      $op = end($trigger['#parents']);
    }
    return $op;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->getOp($form_state) === 'make_booking') {
      $booking_instance_array = $form_state->getValue(['booking', 'booking_instance']);
      $booked_entity = $form_state->get('booked_entity');
      $errors = $this->bookingManager->validateBooking($booking_instance_array, $booked_entity);
      if (!empty($errors)) {
        foreach ($errors as $message) {
          $form_state->setError($form['booking']['booking_instance'], $message);
        }
      }
      else {
        $form_state->setValue(['booking', 'booking_instance'], $booking_instance_array);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $op = $this->getOp($form_state);
    if ($op === 'make_booking') {
      $booking_instance = $this->bookingManager->createBooking($form_state->getValue(['booking', 'booking_instance']));
      $booking_instance->save();
      $this->messenger()->addStatus($this->t('New booking created.'));
    }
    elseif ($op === 'apply_filters') {
      $form_state->setRebuild();
    }
  }

}
