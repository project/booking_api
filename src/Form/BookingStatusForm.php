<?php

namespace Drupal\booking_api\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BookingStatusForm.
 */
class BookingStatusForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $booking_status = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $booking_status->label(),
      '#description' => $this->t("Label for the Booking status."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $booking_status->id(),
      '#machine_name' => [
        'exists' => '\Drupal\booking_api\Entity\BookingStatus::load',
      ],
      '#disabled' => !$booking_status->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $booking_status = $this->entity;
    $status = $booking_status->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Booking status.', [
          '%label' => $booking_status->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Booking status.', [
          '%label' => $booking_status->label(),
        ]));
    }
    $form_state->setRedirectUrl($booking_status->toUrl('collection'));
  }

}
