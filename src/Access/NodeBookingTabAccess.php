<?php

namespace Drupal\booking_api\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\booking_api\Service\BookingManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;

/**
 * Access check for Points user tab route.
 */
class NodeBookingTabAccess implements AccessInterface {

  /**
   * The booking manager service.
   *
   * @var \Drupal\booking_api\Service\BookingManagerInterface
   */
  protected $bookingManager;

  /**
   * Constructs a new NodeBookingTabAccess object.
   *
   * @param \Drupal\booking_api\Service\BookingManagerInterface $bookingManager
   *   The booking manager service.
   */
  public function __construct(
    BookingManagerInterface $bookingManager
  ) {
    $this->bookingManager = $bookingManager;
  }

  /**
   * Checks access to the given user's contact page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Drupal\node\NodeInterface $node
   *   The account being viewed / edited.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, NodeInterface $node) {

    $grant_access = FALSE;

    // Grant access if the current user has any permission defined by this module
    // except the administration permission that is for settings only.
    if ($account->hasPermission('make bookings') || $account->hasPermission('book for all')) {
      if ($this->bookingManager->getSettingsField($node)) {
        $grant_access = TRUE;
      }
    }

    return $grant_access ? AccessResult::allowed() : AccessResult::forbidden();
  }

}
