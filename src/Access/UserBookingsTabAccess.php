<?php

namespace Drupal\booking_api\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access check for Points user tab route.
 */
class UserBookingsTabAccess implements AccessInterface {

  /**
   * Checks access to the given user's contact page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Drupal\user\UserInterface $user
   *   The account being viewed / edited.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, UserInterface $user) {
    if ($account->hasPermission('book for all')) {
      return AccessResult::allowed();
    }
    elseif ($account->id() === $user->id() && $account->hasPermission('make bookings')) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

}
