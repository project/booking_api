Booking API
===========

Provides an API to manage booking of entities.


Getting started
===============

To enable booking, first create a few booking statuses on
/admin/structure/booking_status then add the Booking parameters
field to a chosen entity bundle (node bundles are the most convenient
as they have UI out of the box). After setting the default booking parameters,
a new "Booking" tab appears on the node pages, where one can add, edit and
delete bookings for that node.

The module also provides a user bookings tab, where one can see bookings
of the current user.

The module defines 2 permissions:
  - make bookings: allows normal users to book entities
  - book for all: allows admin users to add, edit and delete bookings of
    other users.


REST endpoint
=============

The endpoint can be accessed by requesting the /api/booking URI (POST, GET, DELETE)

Possible operations:


GET
---

Retrieves a list of booking entities according to given search criteria. Query parameters:
  - entity_type_id (default: node): The entity type ID of the booked entity.
  - entity_id: The ID of the booked entity.
  - user_id: 
  - from: Booking start timestamp, provide an exact value or use an operator (>1575386131).
  - to: Booking end timestamp, provide an exact value or use an operator (<1575486131).
  - status: Booking status or statuses ID, separate multiple values with comma.
  - offset (default: 0): Pager offset.
  - limit (default: 50): Items per page.
  - .. any additional field values if other fields are present on booking entities.

All values support operators (urlencoded) and multiple values:
`some_field=%3E146456` (>146456) or `some_field=value1,value2,value3`

Response contains:
  - total: total number of results for given condition.
  - results: array of bookings keyed by a booking ID.


POST
----

Allows to create a new booking (optionally just check if a booking with certain parameters can be created
for availability checks) or update an existing one.

Request parameters include all fields of a booking instance:
  - id (if provided existing booking will be updated): booking instance ID.
  - user_id (defaults to the current user ID): booking owner user ID.
  - entity_type_id (required on creation): The entity ID of the booked entity.
  - entity_id (required on creation): The ID of the booked entity.
  - from (required or not, according to entity settings): The start timestamp of the booking.
  - to (required or not, according to entity settings): The end timestamp of the booking.
  - status (required on creation): Booking status machine name - see /admin/structure/booking_status.
  - .. any additional field values.

Response code can be:
  - 400 (invalid request with message, also when validating),
  - 403 (access denied with message),
  - 200 (success: update, create or passed validation).


DELETE
------

Use to delete a booking. Request body needs only one parameter: the booking instance ID:
`{"id": "23"}`
