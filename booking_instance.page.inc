<?php

/**
 * @file
 * Contains booking_instance.page.inc.
 *
 * Page callback for Booking instance entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Booking instance templates.
 *
 * Default template: booking_instance.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_booking_instance(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
