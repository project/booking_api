<?php

/**
 * @file
 * Builds placeholder replacement tokens for booking-related data.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Datetime\Entity\DateFormat;

/**
 * Implements hook_token_info().
 */
function booking_api_token_info() {
  $types['booking'] = [
    'name' => t('Bookings'),
    'description' => t('Tokens related to item bookings.'),
    'needs-data' => 'booking_instance',
  ];

  $booking['id'] = [
    'name' => t('Booking ID'),
    'description' => t("The unique ID of the booking instance."),
  ];
  $booking['user'] = [
    'name' => t("User"),
    'description' => t("The name of the user that made the booking."),
  ];
  $booking['item'] = [
    'name' => t("Booked entity label"),
    'description' => t("The label of the booked entity."),
  ];
  $booking['from'] = [
    'name' => t("From date"),
    'description' => t("The date and time the booking starts."),
  ];
  $booking['to'] = [
    'name' => t("To date"),
    'description' => t("The date and time the booking ends."),
  ];
  $booking['status'] = [
    'name' => t("Status"),
    'description' => t("The current status of the booking."),
  ];

  return [
    'types' => $types,
    'tokens' => ['booking' => $booking],
  ];
}

/**
 * Implements hook_tokens().
 */
function booking_api_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  // Add additional token types due to the fact that it's
  // currently impossible to add multiple entities of the
  // same type as tokent replacement data.
  if ($type === 'booking' && !empty($data['booking_instance'])) {

    $replacements = [];
    $booking_instance = $data['booking_instance'];
    if (isset($options['langcode'])) {
      $langcode = $options['langcode'];
    }
    else {
      $langcode = NULL;
    }

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'id':
          // In the case of hook user_presave uid is not set yet.
          $replacements[$original] = $booking_instance->id() ?: t('not yet assigned');
          break;

        case 'user':
          $user = $booking_instance->user_id[0]->entity;
          $bubbleable_metadata->addCacheableDependency($user);
          $replacements[$original] = $user->label();
          break;

        case 'item':
          $entity = \Drupal::service('entity_type.manager')->getStorage($booking_instance->entity_type_id[0]->value)->load($booking_instance->entity_tid[0]->value);
          $replacements[$original] = $entity->label();
          $bubbleable_metadata->addCacheableDependency($entity);
          break;

        case 'from':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = \Drupal::service('date.formatter')->format($booking_instance->from[0]->value, 'medium', '', NULL, $langcode);
          break;

        case 'to':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = \Drupal::service('date.formatter')->format($booking_instance->to[0]->value, 'medium', '', NULL, $langcode);
          break;

        case 'status':
          $status = $booking_instance->status[0]->entity;
          $bubbleable_metadata->addCacheableDependency($status);
          $replacements[$original] = $status->label();
          break;
      }
    }

    return $replacements;
  }
}
